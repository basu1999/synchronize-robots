:-consult("python_lib.pl").
:-consult("platform_ubuntu.pl").
:-consult("handler.pl").
:-consult("list_operations.pl").

:-dynamic leader_selection/0.
:-dynamic leader_execute/0.
:-dynamic leader_handler/3.
:-dynamic leader_acknowledge/3.
:-dynamic leader_executor/3.
:-dynamic choose_leader/1.
:-dynamic starting/3.
:-dynamic elected/1.
:-dynamic ipA/1.
:-dynamic ipB/1.
:-dynamic ipC/1.
:-dynamic start/0.
:-dynamic mainFunc/0.
:-dynamic rssi/1.
:-dynamic send_strength/2. %First payload
:-dynamic send_ip/2. %Second payload
:-dynamic neighbour/1.
:-dynamic ips/1. %stores all the ips
:-dynamic cmip/1. %stores the ip of central command.
:-dynamic send_moves/1.
:-dynamic continue_passing/1.
:-dynamic call_leader/1.
:-dynamic send_clone_agent/3.
:-dynamic move_no/2.
:-dynamic receive_move/3.
:-dynamic move_executor/3.

:-dynamic st1/0.
:-dynamic st2/0.
:-dynamic st3/0.
:-dynamic stCmd/0.
:-dynamic clone_names/1.


% Store important information
% Initialize knowledge base
% Some of these are redundant.
mainFunc:-
    retractall(elected(_)),
    retractall(neighbour(_)),
    asserta(ipA('192.168.1.28')),
    asserta(ipB('192.168.1.38')),
    asserta(ipC('192.168.1.148')),
    asserta(ips('192.168.1.28')),
    asserta(ips('192.168.1.38')),
    asserta(ips('192.168.1.148')),
    asserta(cmip('192.168.1.18')),
    ipA(Fip),ipB(Sip),ipC(Tip),cmip(M),
    get_tartarus_details(Ip,Port),
    writeln(Fip),
    writeln(Sip),
    writeln(Tip),
    writeln(M),
    writeln(Ip),
    (((Ip=@=Fip)->asserta(neighbour(Sip)));
    ((Ip=@=Sip)->asserta(neighbour(Tip)));
    ((Ip=@=Tip)->asserta(neighbour(Fip)));
    ((IP=@=M)->asserta(neighbour('192')))),
    write("Neighbour is= "),neighbour(X),writeln(X),!.


%Passes the list of signal strength from one host to the next.
continue_passing(RecvStr):-
        python_call("rssi_fetch","get_rssi",StrS),
        atom_number(StrS,Str),
        LStr=[Str],
        append(RecvStr,LStr,NewStrL),
        writeln("New Strength list= "),write(NewStrL),writeln(""),
        neighbour(Nbor),
    	write("Neighbour= "),writeln(Nbor),
    	post_agent(platform,(Nbor,6000),[leader_handler,Receiving,(Nbor,6000),NewStrL]),!.


%Will be called by a randomly selected robot at first and then by the leader
leader_selection:-
    get_tartarus_details(IP,Port),
    python_call("rssi_fetch","get_rssi",StrS),
    atom_number(StrS,Str),
    StrL=[Str],
    writeln(StrL),
    neighbour(Nbor),
    write("Neighbour= "),writeln(Nbor),
    write("Neighbour Port= "),writeln(Port),
    post_agent(platform,(Nbor,6000),[leader_handler,Receiving,(Nbor,6000),StrL]),!.



leader_handler(Receiving,(IP,6000),RecvStr):-
    writeln("Leader handler called"),
    write("Strength list= "),write(RecvStr),writeln(""),
    get_tartarus_details(MyIp,Port),
    length(RecvStr,LL),
    write("List length= "),writeln(LL),
    (not(LL=:=3)->
	continue_passing(RecvStr);
        %otherwise
	choose_leader(RecvStr)),!.

%A predicate to send the leader IP to all IPs in the LeaderIPL list.
send_leader_ack([],LeaderIPL).
send_leader_ack([H|T],LeaderIPL):-post_agent(platform,(H,6000),[leader_acknowledge,Receiving,(H,6000),LeaderIPL]),send_leader_ack(T,LeaderIPL),!.


%A predicate to choose leader after all the RSSIs has been collected.
choose_leader(RecvStr):-
        writeln("Choosing leader"),
   	    IpL=['192.168.1.28','192.168.1.38','192.168.1.148'],
        writeln(IpL),
        %Find position of max in the list
        max_list(RecvStr,Max,Index),
        writeln(Index),
        %corresponding IP
        get_tartarus_details(MyIp,Port),
        nth0(MyIpIndex,IpL,MyIp),
        writeln(MyIpIndex),
        LeaderIndex is mod((Index+MyIpIndex),3),
        writeln(LeaderIndex),
        getByIndex(IpL,LeaderIndex,LeaderIp),
        %send the leader ip to all
        LeaderIPL=[LeaderIp],
        writeln(LeaderIPL),
        remove(IpL,MyIp,MIpL),
        writeln(MIpL),
        send_leader_ack(MIpL,LeaderIPL),
        %send it to the laptop
        cmip(MIp),post_agent(platform,(MIp,6000),[leader_acknowledge,Receiving,(MIp,6000),LeaderIPL]),
        %Store the leader
        retractall(elected(_)),
        asserta(elected(LeaderIp)),
        write("Leader is: "),writeln(LeaderIp),
        %Execution
        (MyIp=@=LeaderIp)-> (writeln("Starting execution"),leader_execute,!);
            (writeln("call the leader"),
                %post_agent(platform,(LeaderIpA,7000),[leader_executor,Receiving,(LeaderIpA,6000),[]])),!.
                elected(LeaderIp),
                write("Leader Ip: "),writeln(LeaderIp),
                call_leader([LeaderIp])),!.

%Tell leader to start the dance process
call_leader(LeaderIpL):-
    getid(LeaderIpL,LeaderIp),
    get_new_name(agent,AName),
    get_tartarus_details(MyIp,Port),
    create_mobile_agent(AName,(MyIp,6000),leader_executor,[1]),
    write("Agent created: "),writeln(AName),
    write("Leader Ip: "),writeln(LeaderIp),
    move_agent(AName,(LeaderIp,6000)),
    writeln("Agent moved"),!.

%Ackowledge the leader.
leader_acknowledge(Receiving,(IP,6000),Recvid):-
    retractall(elected(_)),
    getid(Recvid,LIp),
    asserta(elected(LIp)),
    write("Leader ack: "),writeln(LIp),!.

 
 %send a move
send_moves(X):-
     write("Move chosen: "),writeln(X),
     get_tartarus_details(MyIp,Port),
     create_mobile_agent(AName,(MyIP,6000),receive_move,[1]),
     retractall(move_no(_,_)),
     asserta(move_no(AName,X)),
     add_payload(AName,[(move_no,2)]),
     elected(LIp),
     move_agent(AName,(LIp,6000)),
     write("Move agent"),writeln(LIp),!.

%receiver of move
receive_move(Guid,(IP,6000),main):-
    L=[handler_move_forward_backward,handler_rotate_clockwise_anticlockwise,handler_move_right,handler_move_left,stop],
    move_no(Guid,Index),
    getByIndex(L,Index,Handler),
    write("Handler is= "),writeln(Handler),
    get_tartarus_details(MyIp,Port),
    get_new_name(agent,AName),
    create_mobile_agent(AName,(MyIP,6000),Handler,[1]),
    writeln("clonging done"),
    IpL=['192.168.1.28','192.168.1.38','192.168.1.148'],
    remove(IpL,MyIp,MIpL),
    write("MIpL= "),writeln(MIpL),
   send_clone_agent(MIpL,AName,MyIP),
    execute_agent(AName,(MyIp,6000),Handler), %execute
    writeln("3"),
   (Index=:= 4)-> writeln("Done"); leader_selection,!.
    


    

%select a move, create and clone agents and execute those
leader_executor(Receiving,(IP,6000),main):-
    writeln("Moved to leader"),
    leader_execute,!.

%A predicate to recursively clone agents created by the leader and send it to subordinates.
send_clone_agent([],AName,MyIp).
send_clone_agent([H|T],AName,MyIp):- 
    clone_agent(AName, (H, 6000), Clone_name),
    write("Cloning done: "),writeln(Clone_name),
    asserta(clone_names(Clone_name)),
    %move_agent(Clone_name,(H,6000)),
    %write("Moving done: "),writeln(H),
    send_clone_agent(T,AName,MyIp),!.

%A predicate that is executed by the leader to create agents containing moves.
leader_execute:-
    python_call("random_gen","get_random",4,X),
    atom_number(X,Index),
    L=[handler_move_forward_backward,handler_rotate_clockwise_anticlockwise,handler_move_right,handler_move_left], % List of moves
    getByIndex(L,Index,Handler),
    write("Handler is= "),writeln(Handler),
    get_tartarus_details(MyIp,Port),
    create_mobile_agent(originalAgent,(MyIP,6000),Handler,[1]), %Create an agent
    write("Created agent: "),writeln(originalAgent),
    IpL=['192.168.1.28','192.168.1.38','192.168.1.148'],
    write("IpL= "),writeln(IpL),
    remove(IpL,MyIp,MIpL),
    write("MIpL= "),writeln(MIpL),
    send_clone_agent(MIpL,originalAgent,MyIp), %clone and send agents
    writeln("All clones sent!"),
    execute_agent(AName,(MyIp,6000),Handler), %execute
    write(Handler),writeln(" executed!"),
    sleep(8),
    writeln("Sleeping done"),
    leader_selection,!. %Redo the selection process
    
    

cmdStart:- %Randomly select a raspberry pi to start the process
    IpL=['192.168.1.28','192.168.1.38','192.168.1.148'],
    python_call("random_gen","get_random",3,X),
    atom_number(X,XN),
    getByIndex(IpL,XN,IP),
    write(IP),writeln(" Start"),
    post_agent(platform,(IP,6000),[starting,Receiving,(IP,6000),[]]),!.

%Start the leader selection process.
starting(Receiving,(IP,6000),RecvStr):-
    leader_selection,!.

%Alias for platform 1.
st1:-
    start_tartarus('192.168.1.28',6000,1),
    mainFunc,!.
%Alias for platform 2.
st2:-
    start_tartarus('192.168.1.38',6000,1),
    mainFunc,!.
%Alias for platform 3.
st3:-
    start_tartarus('192.168.1.148',6000,1),
    mainFunc,!.
%Alias for command centre.
stCmd:-
    start_tartarus('192.168.1.18',6000,1),!.

