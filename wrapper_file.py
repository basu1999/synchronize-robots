import time
import serial

#f=open('motors.txt','w')

ser=serial.Serial(
                port='/dev/ttyUSB0',
                baudrate=9600,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=2
)

def set_serial(a):
        ser=serial.Serial(

                port='/dev/ttyUSB0',
                baudrate=9600,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=1
        )

def new_read(a):                                  #For Proximity sensor
        ser.write(str(a).encode('utf-8'))                                  #writes to the serial port
        s=ser.read()                                  #reads data from the serial port
        m=int(hex(ord(s)),16)
        return m



def left_white_read(f):                                 #For white line sensors
        ser.write(str(f).encode('utf-8'))
        s=ser.read()
        m=int(hex(ord(s)),16)
        return m


def centre_white_read(g):                                 #For white line sensors
        ser.write(str(g).encode('utf-8'))
        s=ser.read()
        m=int(hex(ord(s)),16)
        return m

def right_white_read(h):                                 #For white line sensors
        ser.write(str(h).encode('utf-8'))
        s=ser.read()
        m=int(hex(ord(s)),16)
        return m

def read_sharp(a):                                     #For sharp IR sensor
        val=[]
        ser.write(str(a).encode('utf-8'))
        #slen=ser.read(10)
        a=ser.read()
        #return ord(a)
        b=ser.read()
        #return ord(b)
        return ord(a)*256+ord(b)
        c=a+b*256
        return c


def move_bot(a):  #a is the speed of both the motors and should be in the format : m100#100!
        ser.write(str(a).encode('utf-8'))
        #f.write(a)
        return a


#EXAMPLES OF USING THE COMMANDS
#move_bot("m150#150!")
#x=new_read("a")                 #to get values from the left-most(robot's left) IR sensor 
#x=new_read("b")                 #similarly use c,d,e
#x=left_white_read("f")          #to get the reading from left white-line sensor
#x=read_sharp("a")               #to get the values from sharp IR sensor
