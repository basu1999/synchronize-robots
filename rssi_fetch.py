import subprocess
#Get rssi infos
def get_rssi():
    results=subprocess.check_output(["iwlist", "wlan0", "scanning"])
    results=results.decode('utf-8')
    for line in results.split('\n'):
        if ("Signal level=" in line):
            #print(line)
            x=line.find("Signal level=")
            x=x+len("Signal level=")
            y=line.find(" dBm")
            val=line[x:y]
            z=val.find("/")
            val=val[:z]
            return val



    

if __name__=='__main__':
    print(get_rssi())
