max_list([X|Xs],Max,Index):-
    max_list(Xs,X,0,0,Max,Index).

max_list([],OldMax,OldIndex,_, OldMax, OldIndex).
max_list([X|Xs],OldMax,_,CurrentIndex, Max, Index):-
    X > OldMax,
    NewCurrentIndex is CurrentIndex + 1,
    NewIndex is NewCurrentIndex,
    max_list(Xs, X, NewIndex, NewCurrentIndex, Max, Index).
max_list([X|Xs],OldMax,OldIndex,CurrentIndex, Max, Index):-
    X =< OldMax,
    NewCurrentIndex is CurrentIndex + 1,
    max_list(Xs, OldMax, OldIndex, NewCurrentIndex, Max, Index).

getByIndex([X], 0, X). 
getByIndex([H|_], 0, H). 
getByIndex([_|T], I, E) :- NewIndex is I-1, getByIndex(T, NewIndex, E).

getid([H|T],I):- I=H.

remove([],X,[]) :- !.
remove([X|T],X,L1) :- remove(T,X,L1), !.
remove([H|T],X,[H|L1]) :- remove(T,X,L1).
