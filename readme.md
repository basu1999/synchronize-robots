HOW TO:-
============================================

0) Assign the following IPs to the 3 raspberry pis' respectively:-
 192.168.1.28, 192.168.1.38, 192.168.1.148

1) Connect to all 3 raspberry pis and create swipl using following command. Also create a swipl in the current host:-
    `swipl`

2) Load the main.pl file on all of them.
    `[main].`

3)Execute st1, st2, and st3 predicates on the 3 raspberry pi's respectively.

4) Execute stCmd on the host machine.

5) Execute cmdStart on the host machine.

