:- consult("platform_ubuntu.pl").
:- consult("python_lib.pl").
:- consult("move1.pl").

:- dynamic kill_agents/1.
:- dynamic handler_rotate_clockwise/3.
:- dynamic handler_rotate_anticlockwise/3.
:- dynamic handler_move_forward/3.
:- dynamic handler_move_backward/3.
:- dynamic check_kill/2.
:- dynamic handler_/3.
:- dynamic sleep_and_kill/0.
:- dynamic handler_move_forward_backward/3.
:- dynamic handler_rotate_clockwise_anticlockwise/3.
:- dynamic handler_move_right/3.
:- dynamic handler_move_left/3.

check_kill(GUID,Myid):-
    not(Myid =@= GUID)->(write("Killed: "),writeln(GUID),purge_agent(GUID));
    true.
kill_agents(Myid):-
    forall(agent_GUID(GUID,_,_),
        check_kill(GUID,Myid)).

%Kill all agents after moves are executed.
sleep_and_kill:-
    forall(agent_GUID(GUID,_,_),(write("Killed: "),writeln(GUID),purge_agent(GUID))),!.

%All following predicates are self explanatory
handler_rotate_clockwise(Guid,(IP,6000),main):-
    writeln("Handler called"),
    move_bot(-25,100).

handler_rotate_anticlockwise(Guid,(IP,6000),main):-
    writeln("Handler called"),
    move_bot(100,-25).

handler_rotate_anticlockwise_backward(Guid,(IP,6000),main):-
    writeln("Handler called"),
    move_bot(-200,-50).


handler_rotate_clockwise_backward(Guid,(IP,6000),main):-
    writeln("Handler called"),
    move_bot(-50,-200).

handler_move_forward(Guid,(IP,6000),main):-
    writeln("Handler forward called"),
    move_bot(140,140).

handler_move_backward(Guid,(IP,6000),main):-
    writeln("Handler backward called"),
    move_bot(-25,-25).

handler_move_forward_backward(Guid,(IP,6000),main):-
    handler_move_forward(Guid,(IP,6000),main),
    sleep(8),
    handler_move_backward(Guid,(IP,6000),main),
    sleep_and_kill,!.

handler_rotate_clockwise_anticlockwise(Guid,(IP,6000),main):-
    handler_rotate_clockwise(Guid,(IP,6000),main),
    sleep(8),
    handler_rotate_clockwise_anticlockwise(Guid,(IP,6000),main),
    sleep_and_kill,!.

handler_move_right(Guid,(IP,6000),main):-
    handler_rotate_clockwise(Guid,(IP,6000),main),
    sleep(8),
    handler_move_forward(Guid,(IP,6000),main),
    sleep_and_kill,!.

handler_move_left(Guid,(IP,6000),main):-
    handler_rotate_anticlockwise(Guid,(IP,6000),main),
    sleep(8),
    handler_move_forward(Guid,(IP,6000),main),
    sleep_and_kill,!.




stop(Guid,(IP,6000),main):-
    writeln("Stopping..."),
    move_bot(0,0).

handler_(Guid,(IP,6000),main):- writeln("hello!!"),
writeln("I am the handler").

