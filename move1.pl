:-use_module(python_lib).
%:-dynamic See/1.
setting:-
	python_call('move','set_serial',1,R),
	writeln('serial comm set').

testing(X,Y):-
	X is Y+1,
	writeln('this is it'),
	python_call('move','read',1,C),
	writeln('The value is ' :C),
	string_to_list(C,A),
	writeln('a is ':A),
	printlist(A).

call_new_read(S3):-
	retractall(s(_)),
	assert(s([])),
	s(S),	
	python_call('move','new_read','a',X1),
	atom_number(X1,X),
	writeln('X is ':X),
	%(isdigit(X),writeln('it is a digit');writeln('not a digit')),
	append(S,[X],S1),
	python_call('move','new_read','b',Y),
	append(S1,[Y],S3),
	writeln('S3 is ':S3),
	printlist(S3).

call_c_third:-
	python_call('move','new_read','c',Y),
	atom_number(Y,Th),
	writeln('Third sensor value is ':Th).


call_sharp:-
	python_call('move','read_sharp','S',Y),
	writeln('Front Sharp value is ':Y),
	python_call('move','read_sharp','T',Z),
	writeln('Left sharp value is ':Z),
	python_call('move','read_sharp','U',A),
	writeln('Right Sharp value is ':A).
	

printlist([]).
printlist([H|T]):-
	writeln('H is ':H),
	printlist(T).

move_bot(M1,M2):-
	M=[1.000,1.000],
	nth0(0,M,TM1),
	nth0(1,M,TM2),
	%M1 is 100*TM1,
	%M2 is 100*TM2,
	%M1 is 200,
	%M2 is 200,
	N1 is ceiling(M1),
	N2 is ceiling(M2),
	atom_concat('m',N1,A),
	atom_concat(A,'#',B),
	atom_concat(B,N2,C),
	atom_concat(C,'!',D),
	writeln('B is ':D),
	python_call('move','move_bot',D,X),
	writeln('X is ':X).	


